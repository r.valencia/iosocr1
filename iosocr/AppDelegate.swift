//
//  AppDelegate.swift
//  iosocr
//
//  Created by Joaquín Alcalde on 3/12/20.
//  Copyright © 2020 T2Company. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    var window: UIWindow?

    func application(
      _ application: UIApplication,
      didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {

      FirebaseApp.configure()
      return true
    }


}

